# SPDX-FileCopyrightText: 2023 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Set the supported macOS versions.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # we only use exports if we really need them

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

# Siril is built exclusively on Monterey
SYS_MACOS_VER_SUPPORTED=(
  12.6.6
  12.6.5
  12.6.4
  12.6.3
  12.6.2
  12.6.1
  12.6
)

# Miminum OS requirement is Catalina on Intel and Big Sur on ARM.
SYS_SDK_VER_SUPPORTED=(
  10.15
  11.3
)

### functions ##################################################################

# Nothing here.

### main #######################################################################

# Nothing here.
